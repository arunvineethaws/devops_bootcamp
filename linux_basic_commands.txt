pwd 					- present working directory
ls 					- list of files and directories
cd/ 					- change directory to root
cd /<directory_name> 	- open the directory
date 				- system date
date -s mm/dd/yyyy		- date change
date -s HH:MM			- time change
arch 				- x86_64 (bit)
uname -a				- kernel version
cat /proc/cpuinfo		- cpu information
df -h				- disk information
free -h				- ram memory information
ifconfig (or) ip a		- ip information
mkdir <foldername>		- create folder
touch <filename>		- create empty file
reboot				- to reboot the machine
rm -rvf <filename>		- to delete the file
rm -rvf <folder>		- to delete the folder
press (ctrl + c)		- terminate the process