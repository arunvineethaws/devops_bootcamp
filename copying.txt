#!/bin/bash

FILENAME="newfile1.txt"
DIRNAME="sample"
DIRNAME1="sample1"

cd /etc/${DIRNAME}
sudo touch ${FILENAME}
cd /etc
sudo mkdir ${DIRNAME1}
sudo cp /etc/${DIRNAME}/* /etc/${DIRNAME1}
cd ${DIRNAME1}
echo file copied to sample1 folder
echo $(ls)
